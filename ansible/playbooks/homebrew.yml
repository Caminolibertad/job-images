---

- name: Fetch Homebrew installer
  get_url:
    url: "{{ homebrew_install }}"
    dest: /tmp/homebrew-install
    mode: "0755"

- name: Install Homebrew
  command: /tmp/homebrew-install
  args:
    creates: "{{ homebrew_repository }}/.git"
  environment:
    NONINTERACTIVE: 1

- name: Load homebrew path for .zshrc
  blockinfile:
    dest: "{{ ansible_user_dir }}/.zshrc"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: brew path"
    block: |
      eval "$({{ homebrew_prefix }}/bin/brew shellenv)"

- name: Load homebrew path for .bash_profile
  blockinfile:
    dest: "{{ ansible_user_dir }}/.bash_profile"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: brew path"
    block: |
      eval "$({{ homebrew_prefix }}/bin/brew shellenv)"

- name: Ensure configured taps are tapped.
  command: "{{ homebrew_prefix }}/bin/brew tap {{ item.name | default(item) }} {{ item.url | default('') }}"
  loop: "{{ homebrew_taps }}"
  environment:
    HOMEBREW_NO_AUTO_UPDATE: 1

- name: Reset homebrew version
  command: git checkout {{ homebrew_cli_version }}
  args:
    chdir: "{{ homebrew_repository }}"

- name: Reset homebrew taps versions
  shell: "(git fetch --unshallow || true) && git checkout {{ item.version }}"
  args:
    chdir: "{{ item.path }}"
  loop: "{{ homebrew_taps_versions }}"

- name: Update homebrew
  # Required to fix "Error: git: undefined method `sha1' for #<SoftwareSpec:0x00007f77ff3d6818>" error while extracting
  command: "{{ homebrew_prefix }}/bin/brew update"
  environment:
    HOMEBREW_NO_INSTALL_FROM_API=1

- name: Extract specific formula versions
  command: "{{ homebrew_prefix }}/bin/brew extract --version {{ item.version }} {{ item.formula }} {{ item.tap }}"
  loop: "{{ homebrew_extracts }}"
  environment:
    # Developer mode required to extract homebrew/homebrew-core
    HOMEBREW_DEVELOPER: 1
  when: homebrew_extracts is defined

- name: Install configured cask applications.
  homebrew_cask:
    name: "{{ item.name | default(item) }}"
    state: present
    install_options: "{{ item.install_options | default(omit) }}"
  loop: "{{ homebrew_cask_apps }}"
  environment:
    HOMEBREW_NO_AUTO_UPDATE: 1

- name: Gather homebrew deps from asdf
  set_fact:
    all_homebrew_packages: "{{ homebrew_installed_packages + asdf_homebrew_deps + (asdf_plugins | selectattr('homebrew_deps', 'defined') | map(attribute='homebrew_deps') | sum(start=[])) }}"

- name: Ensure configured homebrew packages are installed.
  homebrew:
    name: "{{ item.name | default(item) }}"
    install_options: "{{ item.install_options | default(omit) }}"
    state: present
  loop: "{{ all_homebrew_packages | unique }}"
  environment:
    HOMEBREW_NO_AUTO_UPDATE: 1

- name: Set HOMEBREW_NO_AUTO_UPDATE for .zshrc
  blockinfile:
    dest: "{{ ansible_user_dir }}/.zshrc"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: homebrew"
    block: |
      export HOMEBREW_NO_AUTO_UPDATE=1
  when: not homebrew_autoupdate

- name: Set HOMEBREW_NO_AUTO_UPDATE for .bash_profile
  blockinfile:
    dest: "{{ ansible_user_dir }}/.bash_profile"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: homebrew"
    block: |
      export HOMEBREW_NO_AUTO_UPDATE=1
  when: not homebrew_autoupdate

- name: Get google-cloud-sdk caskroom
  command: "{{ homebrew_prefix }}/bin/brew --caskroom google-cloud-sdk"
  register: gdk_caskroom
  changed_when: false

- name: Load google-cloud-sdk in .zshrc
  blockinfile:
    dest: "{{ ansible_user_dir }}/.zshrc"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: gcloud"
    block: |
      source "{{ gdk_caskroom.stdout }}/latest/google-cloud-sdk/path.zsh.inc"
      source "{{ gdk_caskroom.stdout }}/latest/google-cloud-sdk/completion.zsh.inc"

- name: Load google-cloud-sdk in .bash_profile
  blockinfile:
    dest: "{{ ansible_user_dir }}/.bash_profile"
    marker: "# {mark} ANSIBLE MANAGED BLOCK: gcloud"
    block: |
      source "{{ gdk_caskroom.stdout }}/latest/google-cloud-sdk/path.bash.inc"
      source "{{ gdk_caskroom.stdout }}/latest/google-cloud-sdk/completion.bash.inc"
